# -*- coding: utf-8 -*-
from struct import *
import matplotlib.pyplot as plt
import pyaudio
import tkinter
from tkinter.filedialog import askopenfilename
import numpy as np
import cmath
import pylab
from mpl_toolkits.mplot3d import Axes3D

# TODO записать и сохранить звук
# TODO генерация звука изменения параметров звука и сохранение


class NLabel(tkinter.Label):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)


class NButton(tkinter.Button):
    def __init__(self, *args, **kw):
        super().__init__(*args, width=15, height=2, **kw)


class NEtry(tkinter.Entry):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)


class Audio:

    def __init__(self):
        self.file = None
        self.info = {'audioFormat': 0,
                     'numChannels': 0,
                     'sampleRate': 0,
                     'blockAlign': 0}
        self.bytes = []
        self.harmonic = []
        self.rate = []
        self.sinus = []
        self.len = 0
        self.min_amplitude = 50.0
        self.len_signal = 100

    # Открыть файл
    def open(self, path):
        self.file = open(path, 'rb').read()
        self.parse()
        return

    # Создать звук
    def create(self):
        return

    # Записать звук с микрофона
    def record(self):
        return

    # Получить информацию
    def parse(self):
        self.info['audioFormat'] = unpack('h', self.file[20:22])[0]
        self.info['numChannels'] = unpack('h', self.file[22:24])[0]
        self.info['sampleRate'] = unpack('i', self.file[24:28])[0]
        self.info['blockAlign'] = unpack('h', self.file[32:34])[0]
        self.bytes = self.parse_file()
        self.len = round((1 / self.info['sampleRate']) * len(self.bytes), 1)
        self.tones_decay()
        return

    # Обработать данные из файла
    def parse_file(self):
        data = self.file[44:]
        res = []
        block = self.info['blockAlign']
        start = 0
        end = block
        while end < len(data):
            num = unpack('h', data[start:end])[0]
            res.append(num)
            start += block
            end += block
        return res

    # Преобразование фурье
    def tones_decay(self):
        fu = np.fft.fft(self.bytes)
        for signal in fu:
            rec = abs(signal) / len(fu)
            if rec > self.min_amplitude:
                self.harmonic.append(rec)
        self.rate = np.fft.fftfreq(len(self.harmonic), 1. / self.info['sampleRate'])
        self.parse_signals()
        return

    # Обработать полученные сигналы
    def parse_signals(self):
        result = []
        index = 0
        while index < len(self.harmonic):
            signal = {'amplitude': self.harmonic[index], 'sample_rate': self.rate[index]}
            self.sinus.append(self.sine_wave(signal))
            index += 1
        return result

    # Построить гармоничный сигнал
    def sine_wave(self, signal):
        sample_rate = self.info['sampleRate']
        t = 0
        sinus = []
        while t < self.len_signal:
            sinus.append(signal['amplitude'] * np.sin(2 * cmath.pi * t * signal['sample_rate'] / sample_rate))
            t += 1
        return sinus

    # Построить графики
    def graph(self):
        plt.subplot(211)
        plt.title('Временная область')
        plt.plot(range(len(self.bytes)), self.bytes)
        plt.subplot(212)
        plt.title('Частотная область')
        plt.xlabel('Частота')
        plt.ylabel('Амплитуда')
        rate_limit = int(len(self.rate) / 2)
        har_limit = int(len(self.harmonic) / 2)
        plt.plot(self.rate[:rate_limit], self.harmonic[:har_limit])
        plt.show()
        return

    # Изобразить все полученные сигналы
    def graph3d(self):

        fig = pylab.figure()
        axes = Axes3D(fig)
        axes.set_xlabel('x')
        axes.set_ylabel('y')
        axes.set_zlabel('z')
        num = 0
        limit = len(self.sinus) / 2
        for signal in self.sinus[:int(limit)]:
            x = np.linspace(num, num, len(signal))
            y = range(len(signal))
            z = signal
            axes.plot(x, y, z)
            num += 1

        pylab.show()
        return


class Main(tkinter.Tk):

    def __init__(self):
        super().__init__()
        NButton(self, text='Аудио', command=self.modal_audio).grid(row=0, column=0, columnspan=2)
        NLabel(self, text='Код сжатия').grid(row=1, column=0)
        self.audioFormat = NLabel(self, text='')
        self.audioFormat.grid(row=1, column=1)
        NLabel(self, text='Кол. каналов').grid(row=2, column=0)
        self.numChannels = NLabel(self, text='')
        self.numChannels.grid(row=2, column=1)
        NLabel(self, text='Частота').grid(row=3, column=0)
        self.sampleRate = NLabel(self, text='')
        self.sampleRate.grid(row=3, column=1)
        NButton(self, text='График', command=self.audio_wave).grid(row=4, column=0, columnspan=2)
        NButton(self, text='3Д График', command=self.graph3d).grid(row=5, column=0, columnspan=2)
        self.audio = Audio()
        self.resizable(False, False)
        self.root_audio = None

    def print_info(self):
        self.audioFormat['text'] = str(self.audio.info['audioFormat'])
        self.numChannels['text'] = str(self.audio.info['numChannels'])
        self.sampleRate['text'] = str(self.audio.info['sampleRate'])
        return

    def modal_audio(self):
        self.root_audio = tkinter.Toplevel()
        self.root_audio.resizable(False, False)
        NButton(self.root_audio, text='Открыть', command=self.open_file).grid(row=0, column=0)
        NButton(self.root_audio, text='Записать', command=self.record_audio).grid(row=0, column=1)
        NButton(self.root_audio, text='Создать', command=self.create_audio).grid(row=0, column=2)
        return

    def open_file(self):
        path = askopenfilename(filetypes=[('wav file', '*.wav')])
        if len(path) == 0:
            return
        self.audio.open(path)
        self.print_info()
        self.root_audio.destroy()
        return

    def record_audio(self):
        self.root_audio.destroy()
        return

    def create_audio(self):
        self.root_audio.destroy()
        return

    def audio_wave(self):
        self.audio.graph()
        return

    def graph3d(self):
        self.audio.graph3d()
        return

if __name__ == '__main__':
    Main().mainloop()
